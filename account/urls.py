from django.conf.urls import url
from django.contrib.auth.views import login, logout

from . import views

app_name = 'account'
urlpatterns = [
	url(r'^registration/$', 
		views.register, 
		name='registration'),

	url(r'^login/$', 
		login, 
		name='login'),

	url(r'^logout/$', 
		logout, 
		name='logout'),
]