# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.views.generic import FormView, TemplateView, ListView
from django.shortcuts import render_to_response
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from .forms import TaskForm
from .models import Task


def register(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponse('/account/registration_form')

	else:
		form = UserCreationForm()
	token = {}
	token['form'] = form
	return HttpResponse('/account/registration_form')


def registration(request):
	return render_to_response('registration/registration.html')

def login(register):
	return render_to_response('registration/login.html',
							  {'username': request.user.username})

def logout(request):
	return render_to_response('registration/logout.html')

def index(request):
	return render_to_response('main/index.html')

# class TaskListView(FormView):
# 	template_name = 'account/task.html'




class TaskForm(FormView):
	template_name = 'index.html'
	form_class = TaskForm

	def form_valid(self, form):

		form = TaskForm()
		context = {
			"form":form
		}
		return super(TaskForm, self).form_valid(form)
