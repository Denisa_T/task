from django import forms

from .models import Task


class TaskForm(forms.ModelForm):

		task_name = forms.CharField(max_length=30)
		description = forms.CharField(max_length=100)
		date = forms.DateTimeField()
		crate_modify_date = forms.DateTimeField()