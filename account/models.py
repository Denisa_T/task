from __future__ import unicode_literals

from django.db import models


class User(models.Model):
	first_name = models.CharField(max_length= 50)
	last_name = models.CharField(max_length= 50)
	

class Task(models.Model):
	task_name = models.CharField(max_length=30)
	description = models.CharField(max_length=100)
	date = models.DateTimeField()
	crate_modify_date = models.DateTimeField()
