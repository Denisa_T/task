from django.conf.urls import include, url
from django.contrib import admin
from account import views
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [

	url(r'^account/', include('account.urls')),
	# url(r'^task/', include('Task.urls')),
	url(r'^admin/', admin.site.urls),
	url(r'^$', views.index, name='index'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
